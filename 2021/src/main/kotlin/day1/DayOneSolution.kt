package day1

import core.ResultLog
import core.loadFileAsText

data class CountTrack(
    val previousNumber: Int? = null,
    val totalIncreases: Int = 0
)

fun challengeOne(filePath: String) = filePath.loadFileAsText { f ->
    f.split("\n")
        .map { it.toInt() }
        .fold(CountTrack()) { acc, next ->
            val isIncrease = acc.previousNumber?.run { next > this } ?: false
            val newTotal = if (isIncrease) acc.totalIncreases + 1 else acc.totalIncreases
            CountTrack(next, newTotal)
        }.totalIncreases.toString()
}

fun challengeTwo(filePath: String) = filePath.loadFileAsText { f ->
    val numberList = f.split("\n").map { it.toInt() }

    numberList.mapIndexed { idx, i ->
        if (idx < numberList.size - 2) i + numberList[idx + 1] + numberList[idx + 2]
        else null
    }
        .filterNotNull()
        .fold(CountTrack()) { acc, next ->
            val isIncrease = acc.previousNumber?.run { next > this } ?: false
            val newTotal = if (isIncrease) acc.totalIncreases + 1 else acc.totalIncreases
            CountTrack(next, newTotal)
        }.totalIncreases.toString()
}


fun main() {
    ResultLog(
        challengeOne("/1/s.txt"),
        challengeOne("/1/1.txt"),
        challengeTwo("/1/s.txt"),
        challengeTwo("/1/1.txt")
    )
}