package day2

import core.ResultLog
import core.loadFileAsText

enum class Direction {
    Forward,
    Up,
    Down;

    companion object {
        fun fromString(value: String) = when(value) {
            "forward" -> Forward
            "up" -> Up
            "down" -> Down
            else -> throw IllegalStateException("Invalid value for Direction: $value")
        }
    }
}

data class Position(
    val horizontal: Int = 0,
    val depth: Int = 0,
    val aim: Int = 0
) {
    operator fun plus(other: Position) = Position(horizontal + other.horizontal, depth + other.depth, aim + other.aim)
}

data class NavigationStep(
    val direction: Direction,
    val amount: Int
) {
    val position = when (direction) {
        Direction.Forward -> Position(horizontal = amount)
        Direction.Up -> Position(depth = -amount)
        Direction.Down -> Position(depth = amount)
    }

    fun positionWithAim(pos: Position) = when(direction) {
        Direction.Forward -> Position(amount,pos.aim * amount)
        Direction.Up -> Position(aim = -amount)
        Direction.Down -> Position(aim = amount)
    }
}
fun one(path: String) =
    path.loadFileAsText {
        it.split("\n")
            .map { line ->
                val (directionString, num) = line.split(" ")
                NavigationStep(
                    Direction.fromString(directionString),
                    num.toInt()
                )
            }.fold(Position()) {acc, next ->
                acc + next.position
            }.run {
                horizontal * depth
            }

    }

fun two(path: String) =
    path.loadFileAsText {
        it.split("\n")
            .map { line ->
                val (directionString, num) = line.split(" ")
                NavigationStep(
                    Direction.fromString(directionString),
                    num.toInt()
                )
            }.fold(Position()) { acc, next ->
                acc + next.positionWithAim(acc)
            }.run {
                println("[h=$horizontal,d=$depth]")
                horizontal * depth
            }
    }

fun main() {
    ResultLog(
        one("/2/s.txt").toString(),
        one("/2/2.txt").toString(),
        two("/2/s.txt").toString(),
        two("/2/2.txt").toString()
    )
}