package core


private const val NOT_DONE = "NOT DONE"

/**
 * Helper class for printed a formatted result
 */
data class ResultLog(
    val sampleOne: String = NOT_DONE,
    val challengeOne: String = NOT_DONE,
    val sampleTwo: String = NOT_DONE,
    val challengeTwo: String = NOT_DONE
) {

    init {
        printResult()
    }

    fun printResult() {
        println("""
        Challenge 1
        ============================
        Sample Result: $sampleOne
        Day One Result: $challengeOne
        ============================
        Challenge 2
        ============================
        Sample Result: $sampleTwo
        Day Two Result: $challengeTwo            
        """.trimIndent())
    }
}