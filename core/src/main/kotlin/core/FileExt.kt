package core

/**
 * Helper function to load a file from resources from a path
 * @param [work] Function that will process the text loaded from file
 * @return [T] the result of the [work] function
 */
fun <T> String.loadFileAsText(work: (text: String) -> T): T {
    val obj = object {}
    return work(obj.javaClass.getResource(this)!!.readText())
}