package core

import org.amshove.kluent.shouldNotBeEmpty
import org.junit.jupiter.api.Test

class TestReadString {
    @Test
    fun testRead() {
        val result = "/1/s.txt".loadFileAsText {
            it
        }

        result.shouldNotBeEmpty()
    }
}

