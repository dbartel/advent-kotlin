package day1

import core.ResultLog
import core.loadFileAsText

data class Elf(
    val calorieList: List<Int>,
    val totalCalories: Int = calorieList.sum()
)

fun buildElfList(data: String): List<Elf> {
    val calorieList = mutableListOf<Int>()
    val elfList = mutableListOf<Elf>()

    val calList = data.split("\n")

    calList.forEachIndexed { idx, cal ->
        if (cal == "" || idx == calList.size - 1) {
            elfList.add(Elf(calorieList.toList()))
            calorieList.clear()
        } else {
            calorieList.add(cal.toInt())
        }
    }
    return elfList
}

fun challengeOne(filePath: String): String = filePath.loadFileAsText { f ->
    buildElfList(f).maxByOrNull { it.totalCalories }!!.totalCalories.toString()
}

fun challengeTwo(filePath: String) = filePath.loadFileAsText { f ->
    val elves = buildElfList(f)
        .sortedByDescending { it.totalCalories }
        .slice(0..2)

    println("Count ${elves.size}")

    elves.forEach {
        println(it.totalCalories)
    }

    elves
        .sumOf { it.totalCalories }
        .toString()
}

fun main() {
    ResultLog(
        challengeOne("/1/s.txt"),
        challengeOne("/1/i.txt"),
        challengeTwo("/1/s.txt"),
        challengeTwo("/1/i.txt")
    )
}