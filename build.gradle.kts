
plugins {
    kotlin("jvm") version "1.6.0"
}

allprojects {
    apply(plugin = "java")
    apply(plugin = "kotlin")

    repositories {
        mavenCentral()
    }

    tasks.withType<Test>().configureEach {
        useJUnitPlatform()
    }

    if (name != "core") {
        dependencies {
            implementation(project(":core"))
        }
    }

    dependencies {
        testImplementation(platform("org.junit:junit-bom:5.8.2"))
        testImplementation("org.junit.jupiter:junit-jupiter")
        testImplementation("org.amshove.kluent:kluent:1.68")
    }
}

